import { Component, OnInit,Input } from '@angular/core';
import {CAvatar} from '../servicios/avatar';
import {SAvatarService} from '../servicios/savatar.service'
import {trigger,state,style,animate,transition} from '@angular/animations';


@Component({
  selector: 'app-mostrar',
  templateUrl: './mostrar.component.html',
  styleUrls: ['./mostrar.component.css']
})
export class MostrarComponent implements OnInit {

  @Input() Servicio:SAvatarService;

  Avatars:CAvatar[];

  constructor() { }

  ngOnInit() {
    this.Avatars=this.Servicio.GetAvatars();
  }

}
