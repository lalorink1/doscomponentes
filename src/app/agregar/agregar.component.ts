import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {CAvatar} from '../servicios/avatar';
import {SAvatarService} from '../servicios/savatar.service'
import {trigger,state,style,animate,transition} from '@angular/animations';
declare var $:any;

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css'],
  animations:[
    trigger('cambioForma',[
      state('inactivo',style({
        backgroundColor:'#eee',
        transform:'scale(1)'
      })),
      state('activo',style({
        backgroundColor:'#dada',
        transform:'translateX(100px)'
      })),
      transition('inactivo => activo',animate('100ms ease-in')),
      transition('activo => inactivo',animate('100ms ease-out'))
    ])
  ]
})
export class AgregarComponent implements OnInit {
  public estado:string="inactivo";

  validado:boolean=true;


  Avatar:CAvatar;
  Avatars:CAvatar[];
  constructor(private SAvatarService:SAvatarService) { }


  
  ngOnInit() {
      
    }

  onSubmit(NgForm:NgForm){
    this.Avatar=NgForm.value;
    this.SAvatarService.AgregarAvatar(this.Avatar);
    NgForm.reset();
  }
    
  SeleccionarIcon(){
    this.estado=this.estado==='inavtivo' ? 'activo':'inavtivo';

    // $(".icon").removeClass('fa-hand-pointer'); 
  }

}
