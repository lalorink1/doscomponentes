import { Injectable } from '@angular/core';
import {CAvatar} from './avatar'

@Injectable({
  providedIn: 'root'
})
export class SAvatarService {

  icon:any[]=["fa fa-motorcycle","fa fa-puzzle-piece","fa fa-paper-plane","fa fa-star","fa fa-life-ring"]
  JsonAvatar:CAvatar[]=[];

  constructor() { }

  public AgregarAvatar(avatar:CAvatar):void{
    avatar.icon=this.icon[Math.floor(Math.random() * 5) + 0]
    this.JsonAvatar.push(avatar);
  }

  public GetAvatars(){
    return this.JsonAvatar;
  }

}
